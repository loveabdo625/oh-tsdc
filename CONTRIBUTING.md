<!--
SPDX-FileCopyrightText: 2021 Martin Häuer <martin.haeuer@ose-germany.de>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Repo information

- Please work directly in the RDF (master).
- The only purpose of the markdown output \[filename & link\] is visualization,
  e.g. in order to get feedback from contributors which are not familiar with RDF
  (and refuse learning to read it).
- We also accept final thesis', PhD's and internships ;)

## Generated content

Graph representations (logical and visual) of the RDF part of the standard
are generated after each commit to this repo,
and are stored [on the repos pages](https://osegermany.gitlab.io/oh-tsdc/).
