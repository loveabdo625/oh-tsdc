<!--
SPDX-FileCopyrightText: 2021 Martin Häuer <martin.haeuer@ose-germany.de>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# OH-TsDC

## Introduction

## This document

This documents contains Technology-specific Documentation Criteria (TsDC)
according to [DIN SPEC 3105-1](https://gitlab.com/OSEGermany/OHS-3105/-/blob/ohs/DIN_SPEC_3105-1.md)
in modular form. Each module bears an identifier (TsDC-ID) so requirements can be
unambiguously referenced. For that, this document is automatically exported into
machine-readable Linked Open Data (OWL, see the TTL file in this repository).

### Starting point

Imagine we're working on some fairly simple magical machine –
everything open source of course.
But what does "open source" actually mean here?
Are we expected to provide design files for _every_ part of our machine?
Should people be able to wrap capacitors by themselves?

Well, of course not.

Naturally it's fine to include proprietary (or standard) components in our design,
that people would just need to buy.\
For components we designed by ourselves (hence which people cannot just buy anywhere)
we must provide the original design files
(plus an export so they have a fair chance to access the information
without special or expensive software).\
This obviously includes assemblies.\
Exercising best practices of open source, our magical modular machine also
includes open source components someone else designed. Since this marvellous
person already shared the necessary documentation, it's enough when we just link
to the corresponding release we used in our design.

As we see, this already gives us 3 different types of components (COM)
for which different documentation requirements apply:

- proprietary components (BUY)
- standard components (STD)
- self-designed components to manufacture (MAN)

Plus assemblies (ASM).

The present document defines those technology-specific documentation criteria
(TsDC) and structures them in modular form by the use of identifiers (TsDC-ID),
such as COM-STD for standardised components.

A simplified bill of materials for the general assembly of our magical machine
may look like this:

| Pos. | Name         | Units | TsDC-ID | Reference                                                                     |
|------|--------------|-------|---------|-------------------------------------------------------------------------------|
| 1    | casing       | 2     | COM-MAN | link to [POSH file](ohs-3105-3#piece-of-osh-posh) which links to design files |
| 2    | screw        | 4     | COM-STD | hexagon screw ISO 4017 – M6 × 35 – 8.8                                        |
| 3    | gear box     | 1     | COM-OSH | link to corresponding release [OSH-Module](ohs-3105-3#osh-module)             |
| 4    | Raspberry Pi | 1     | COM-BUY | unambiguous reference (not standardised)                                      |
| 5    | holder       | 1     | ASM-MEC | link to [POSH file](ohs-3105-3#piece-of-osh-posh) which links to design files |
| 5.1  | bracket      | 2     | COM-MAN | link to [POSH file](ohs-3105-3#piece-of-osh-posh) which links to design files |
| 5.2  | screw        | 2     | COM-STD | hexagon screw ISO 4017 – M4 × 12 – 5.6                                        |
| 5.3  | arm          | 2     | COM-MAN | link to [POSH file](ohs-3105-3#piece-of-osh-posh) which links to design files |
| 6    | controller   | 1     | COM-OSH | link to corresponding release [OSH-Module](ohs-3105-3#osh-module)             |

Of course, there's more.
Welded assemblies will need other documents than PCBs (which are also assemblies).
Plus there're are zillion types of post-processing one can do
to components and assemblies – including software installation.

## Scope

The definition and explanation of Technology-specific Documentation Criteria (TsDC)
according to DIN SPEC 3105-1.

The stated requirements, specifically the recommended form of their provision,
give a base for automatically exported
machine-readable Linked Open Data which facilitates their practical adoption –
specifically their integration into the metadata standard (coming OSH 3105-3)
and the graph database for open source hardware.

Wherever possible (and reasonable) official standards are quoted,
referenced or used as an inspiration.

## Normative references

The categorisation of requirements does not follow, but is inspired by DIN 8593.

## Terms and definitions

<!--
copy from GINP
and LOSH TTL
-->

| term | definition | in simple words |
| ---- | ---------- | --------------- |
| component |||
| manufactured component |||
| standard component |||
| proprietary component |||
| assembly |||
| post-processing || anything done to the hardware after it has been made or bought |

## General requirements

(…potentially to be added to DIN SPEC 3105-1)

**Rationale:**
In some cases a rationale is necessary to fully understand
the way how components, assemblies or post-processings are bound to each other
and which decisions led to their choice.
If external engineers would try to e.g. modify the overlying assembly
and (significant) additional research would be required,
a (brief) rationale is mandatory.
Data sheets, calculations or referenced discussions are excellent additions
to such rationales.

``` JSON
rationale
```

## Technology-specific Documentation Criteria

Legend:

`TsDC-ID`

``` JSON
list of
data fields
for linked open data
```

**definition of TsDC module**

- list of
- requirements
  - that may be reshaped as continuous text in the future

### component

#### generic

`PRT`

``` JSON
design
manufacturing-instruction
```

**self-designed component**

- geometry
  - if necessary:
    - tolerances
    - surface specifications
- material properties
  - so others can repair and recycle
    \[…\] physical realisations of this documentation release
  - can be a generic statement (e.g. construction steel) leastwise
- if necessary:
  - manufacturing specifications (e.g.  manufacturing process, machine settings)

Best practice of files to provide:

- source files
  - 3D models &/ 2D drawings in their original file format (e.g. FCStd)
  - rationale in their oritinal file format (e.g. ODT)
- export files for production
  - 3D printing
    - 3D STL model
  - laser cutting
    - SVG drawing
  - anything else
    - 3D models in a standard exchange format (e.g. STEP, JT, IGES)
    - 2D drawings a standard exchange format (e.g. PDF, SVG)

#### open source hardware

`OSH`

``` JSON
manifest-file
```

**component or assembly that fully complies with DIN SPEC 3105-1**

- unambiguous reference to the corresponding documentation release

#### standard component

`STD`

``` JSON
standard-designation
```

**component or assembly that is officially standardised**

- unambiguous reference by standard designation

EXAMPLE: hexagon screw ISO 4017 – M12 × 80 – 8.8

See [this well documented OSH design](https://github.com/opensourceimaging/cosi-measure/blob/master/Mechanical%20System/2018-08-24_cosi-measure_v1.0_BoM_mechanical.ods)
as a reference.

Note 1 to entry:
Please don't just say "M6 screw".
The more you specify, the less brain power people must investw
before they can effectively rebuild or reuse your design.

#### proprietary component

`BUY`

``` JSON
reference-buy
```

**component or assembly that is neither officially standardised
nor fully compliant with DIN SPEC 3105-1 and hence just bought**

- unambiguous reference so others have a fair chance to obtain
  sufficient information about the component and how to obtain it

EXAMPLE:

See [this well documented OSH design](https://libre.solar/mppt-1210-hus/mppt-1210-hus_ibom.html)
as a reference.

Note 1 to entry:
Please don't just say "diode" or "capacitor".
The more you specify, the less brain power people must invest
before they can effectively rebuild or reuse your design.

#### 3D-printed

`3DP`

please define:

- geometry (by provided 3D models)
  - if necessary:
    - tolerances
    - surface specifications
- material properties
  - reference of material used for this part
  - can be a generic statement (e.g. PLA) leastwise
- printing-process
  - FDM, SLA, SLS, MJF, DMLS
- if necessary:
  - manufacturing specifications (e.g. special manufacturing process, machine settings)

Best practice of files to provide:

- source files
  - 3D models in their original file format (e.g. SCAD)
  - rationale in its original file format (e.g. MD)
- export files
  - for production: in STL format
  - for exchange and integration in assemblies: in a standard exchange format (e.g. STEP, JT, IGES)

Best practice for further metadata:

- `infill` [float]
  - print parameter: infill (in %)
- `raft-brim` [bool]
  - 0 = design has no raft or brim
  - 1 = design includes raft or brim
- `supports` [bool]
  - 0 = design has no supports
  - 1 = design includes support(s)
- `resolution-mm` [float]
  - print parameter: resolution/layer height in mm
- `shell-thickness` [float]
  - shell thickness in mm
- `top-bottom-thickness` [float]
  - top/bottom thickness in mm

#### laser-cutted

please define:

- geometry (by provided 2D models)
  - if necessary:
    - tolerances
    - surface specifications
- material properties
  - reference of material used for this part
  - can be a generic statement (e.g. acrylic glass) leastwise
- if necessary:
  - manufacturing specifications (e.g. special manufacturing process, machine settings)

Best practice of files to provide:

- source files
  - 2D models in their original file format (e.g. SVG)
  - rationale in its original file format (e.g. MD)
- export files
  - for production: exported pattern in common exchange format (e.g. SVG)
    - NOTE to entry: if your source file _is_ already in SVG format, no additional copy is required, of course

Best practice for further metadata:

- `engraving-depth-mm` [float]
  - depth of engraving in mm in case the part has an engraving
  - e.g. `0.25`
- `resolution-dpi` [integer]
  - resolution of engraving in DPI
  - e.g. `500`
- `thickness-mm` [float]
  - thickness of the sheet material in mm the laser is supposed to cut through
  - e.g. `1.5`

#### CNC-milled

please define:

- geometry (by provided 3D &/ 2D models)
  - if necessary:
    - tolerances
    - surface specifications
- material properties
  - reference of material used for this part
  - can be a generic statement (e.g. construction steel) leastwise
- if necessary:
  - manufacturing specifications (e.g.  manufacturing process, machine settings)

Best practice of files to provide:

- source files
  - 3D models & 2D drawings in their original file format (e.g. FCStd)
  - rationale in their oritinal file format (e.g. MD)
- export files
  - for production: technical drawing in common exchange format (e.g. PDF)
  - for exchange and integration in assemblies: in a standard exchange format (e.g. STEP, JT, IGES)

Best practice for further metadata:

- `smallest-tolerance-class` [string]
  - smallest tolerance class of measures according to ISO 286
  - e.g. `IT9`
- `smallest-inner-radius-mm` [float]
  - smallest inner radius of corners in mm
  - e.g. `0.5`

### assembly

`ASM`

``` JSON
bom
design
list-auxiliary-components
list-special-tools
manufacturing-instruction
```

**assembly of components**

general requirements:

- involved components are referenced in a bill of materials (see best practices [here](https://gitlab.com/OSEGermany/OHS-3105-aux/-/blob/master/DIN_SPEC_3105-1_best-practice.md#bill-of-materials))
- geometry
- position of junctions and joining technology specified
- if necessary:
  - assembly instructions

#### mechanical

`MEC`

``` JSON
calibration plan
```

if general assembly requirements are not enough
the following requirements apply if applicable:

- calibrations ideally in a calibration plan
- list of required special tools
- list of additional materials (e.g. lubricants)

#### welded

`WEL`

``` JSON
welding-sequence-plan
welding-specification
```

if general assembly requirements are not enough
the following requirements apply if applicable:

- welding seam geometry, welding process and filler metal
  (including welding seam preparation)
  - defined in assembly drawing, welding specification
- welding sequence
  - defined in welding sequence plan

#### printed circuit board

`PCB`

- interconnection between components
  - \[circuit diagram\]
    - alternative designation: schematic
- PCB layout
  - \[PCB overlay diagram\]
    - alternative designation: board layout
  - special direction or polarity of components are specified
- all related components, pins or connectors are unambiguously and consistently
  referenced among all files (e.g by IDs or serial numbers)

EXAMPLE: \[mehrdad's pictures\]

#### electronic circuitry

`CIR`

- interconnection between components
  - \[circuit diagram\]
    - alternative designation: schematic
- all related components, pins or connectors are unambiguously and consistently
  referenced among all files (e.g by IDs or serial numbers)

EXAMPLE: \[some classic arduino project\]

### post-processing

`POST`
