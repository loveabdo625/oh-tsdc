<!--
SPDX-FileCopyrightText: 2021 Martin Häuer <martin.haeuer@ose-germany.de>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# TsDC Questionnaire

## My hardware includes:

- [ ] a PCB\
`ASM-PCB`
- [ ] any other assembly\
`ASM-GEN`
    - [ ] …including mechanically joined components\
_e.g. by screws, nuts, press-fit_\
`ASM-MEC`
    - [ ] …including welded components\
`ASM-WEL`

## My bill of materials includes:

- [ ] components to be manufactured\
_So we obviously need design files._\
`COM-MAN`
- [ ] standard components\
_components defined by an official standard (e.g. screws)_\
`COM-STD`
- [ ] other OSH\
_…according to DIN SPEC 3105-1_\
`COM-OSH`
- [ ] proprietary components\
`COM-PROP`

## And yes, I do have post-processing involved:
- [ ] coating/filling\
_e.g. painting_\
`POST-COT`
- [ ] change of material properties\
_e.g. hardening, soft annealing_\
`POST-PRO`
- [ ] IT setup\
_e.g. software/firmware installation_\
`POST-IT`