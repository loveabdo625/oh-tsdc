<!--
SPDX-FileCopyrightText: 2021 Martin Häuer <martin.haeuer@ose-germany.de>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# CONTRIBUTING-REVIEWERS

## General note

**THANKS**

You just chose join a group of essential supporters
and fuel the global movement of open source hardware
with your professional expertise. By providing constructive feedback,
you are not only supporting transparency and trustfulness
of the term "open source hardware" but also improving
the overall quality of technical documentation,
the “source code” of open source hardware.

This document shall give you a quick’n’dirty introduction to the process.
To look up details or to fulfill a strong desire for technical notation,
please check the DIN SPEC 3105-2 (link) or get in touch with us (here or there).

## Scope

DIN SPEC 3105-1, DIN SPEC 3105-2 and the TsDC provide enforceable requirements
for technical documentation of OSH.
Developers may want to comply with them and someone else
(the community) assesses that.
A "conformity assessment body" moderates the whole process so

## The procedure: community-based assessment

### Roles & duties

The following terms are copied from DIN SPEC 3105-2
and needed to be aligned to definitions in international standards.
Wording may appear weird in the context of OSH :)

- client
  - submits a stable documentation release to the CAB
    in order to get it reviewed and assessed according to DIN SPEC 3105-2
  - point of contact for CAB and reviewers for questions and feedback
    regarding the submitted documentation
- reviewer
  - 3rd party
  - Reviews are (almost) anonymous.
    The CAB is able to view the contact details of reviewers
    and may ask reviewers to make a public declaration
    on their conflicts of interest regarding the outcome of the assessment.
- conformity assessment body, CAB (e.g. OHO)
  - all actions in the assessment process are performed online and are publicly visible.
    This means that all information relative to the assessment process
    can be viewed online by anyone without any restricted access
    and is released under a free/open license.
  - → which means that the CAB is "forkable" for anyone anytime

NOTE:
As a reviewer don‘t need to review the whole documentation.
Especially in case of large and complex assemblies
it‘s very unlikely to be a specialist in all technologies embedded in the piece of hardware.
Please review the sections that match your background
and share your expertise with the community;
don‘t feel pressured to review documents that escape your technical competence.

### Process steps

#### tl;dr

[graphic] (OSH Documentation v1.0)
→ (reviewing by the community)
→ (approval by the community)
→ (attestation)

### Actual steps

1. Client submits documentation release to CAB
2. CAB
    1. checks some formalities and
    2. opens the review process.
    3. CAB may invite selected reviewers in order to support the discussion
3. Reviewers
    1. Check whether the documentation contains sufficient information
       so that users could effectively understand, reproduce, modify and operate the piece of hardare.
       Note that users/recipients are meant to be specialists
       in the corresponding field of technology (e.g. mechanical or electrical engineers).
       However, projects may define _additional_ (propably larger) target groups.
    2. Take a decision for each document they have checked:
        1. "Approved"
           Document is good to go from your perspective.
        2. "Approval subject to revision"
           You missed some important information
           or had serious issues understanding the document.
           → Please provide constructive feedback so the authors can fix this.
4. As soon as the whole documentation release has been approved at least twice,
   the CAB can issue an attestation stating the compliance with the DIN SPEC 3105-1.
   Reviewing is closed by then (but may be reopened)

After the CAB has issued the attestation, anyone can submit complaints to the CAB.
A complaint may, among others, be triggered in case:

- the documentation release is not accessible anymore or parts of it disappeared,
- the licensing terms have been changed and are not compliant anymore
  with the requirements of the DIN SPEC 3105-1 or
- any other relevant information has been changed or disappeared.

Considering the importance of submitted complaints,
the CAB voids the attestation and reopens the documentation release again for reviewing.

#### Documentation updates

Clients may submit updates of already attested documentation release.
Identical, so already attested, hence approved, parts of the documentation
keep their validity and don't need to be reviewed again.

## General requirements to check

(for everyone)
[DIN SPEC 3105-1]

1. has been released under licensing terms complying with the OSHWA Definition 1.0
   (e.g. CERN OHL v2)
2. bears references of:
    1. its authors;
    2. a functional description of the piece of OSH,
       e.g. what functions it is supposed to deliver,
       what is the problem it solves, for whom, etc.;
    3. a mention of all applying technology-specific documentation criteria (see blow);
    4. all documents required by the mentioned technology-specific documentation criteria;
    5. a name or working title given to the piece of OSH.

### Technology-specific requirements to check

(for everyone with sufficient knowledge in the corresponding field of technology):
TsDC
